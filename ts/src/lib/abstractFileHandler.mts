import NodePath from 'node:path';
import NodeFs from 'node:fs';
import NodeUrl from 'node:url';

import { Nodir } from '@cyberjs.on/nodir';
import { FileForklift } from '@cyberjs.on/file-forklift';


export abstract class AbstractFileHandler<T> {

    protected path!: Nodir.Path;

    protected url!: NodeUrl.URL;

    protected abstract treatContent2Record(content: T, ...args: any[]): string;

    protected abstract treatObtainedContent(content: string): T | null;

    constructor(pathname: string) {

        Nodir.Path.operatingSystemPathSeparator = NodePath.sep;

        /**
         *
         * if the path is absolute
         */
        if (NodeFs.existsSync(pathname)) {
            this.path = new Nodir.Path(pathname);
        } else {

            this.path = this.#resolveCallerPath(pathname);

            if (!NodeFs.existsSync(this.path.toString())) {
                throw new Error(`The file ${this.path.toString()} doesn't exist.`);
            }
        }

        this.url = new NodeUrl.URL(`file://${this.path.toString()}`);
    }

    write(content: T, ...args: any[]): Promise<void> {

        let _content: string;

        let _args = Array.from(arguments);

        if (_args.length > 1) {
            _args = _args.slice(1, _args.length);
            _content = this.treatContent2Record(content, ..._args);
        } else {
            _content = this.treatContent2Record(content);
        }

        return NodeFs.promises.writeFile(this.url, _content!);
    }

    async read(): Promise<T | null> {

        const content = await this.#readContent(this.url);

        let traitedContent = this.treatObtainedContent(content);

        return Promise.resolve(traitedContent);
    }

    async #readContent(url: NodeUrl.URL): Promise<string> {

        const buffer = await NodeFs.promises.readFile(url);

        let content = buffer.toString();

        if (!content) {
            content = '';
        }

        return Promise.resolve(content);
    }

    #resolveCallerPath(pathname: string): Nodir.Path {

        const fileProtocolImportTracker = new FileForklift.FileProtocolImportTracker;

        let callerFilePath: Nodir.Path;

        let callerFileDirectoryname: string;

        callerFilePath = new Nodir.Path(fileProtocolImportTracker.callerUrlname);

        callerFileDirectoryname = callerFilePath.withoutLowerFloors(1);

        return  new Nodir
            .Path(Nodir.Path.relativeResolve(Nodir.Path.join(callerFileDirectoryname, pathname)));
    }

}
