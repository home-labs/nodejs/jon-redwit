export class EcmaScriptFilesType {

    static get resolveBlockCommentsPattern() {
        return /\/\*.+?\*+\//gs;
    }

    static get resolveLineCommentsPattern() {
        return /\/\/.*\n*/g;
    }

}
