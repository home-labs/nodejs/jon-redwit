import { AbstractFileHandler } from './abstractFileHandler.mjs';


export class FileHandler extends AbstractFileHandler<string> {

    constructor(pathname: string) {
        super(pathname);
    }

    override write(content: string) {
        return super.write(content);
    }

    protected treatContent2Record(content: string): string {
        return content;
    }

    protected treatObtainedContent(content: string): string {
        return content;
    }

}
