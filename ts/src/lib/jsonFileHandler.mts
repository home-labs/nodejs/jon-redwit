import { AbstractFileHandler } from './abstractFileHandler.mjs';
import { Helpers } from './index.mjs';


export class JsonFileHandler<T extends object> extends AbstractFileHandler<T> {

    constructor(pathname: string) {
        super(pathname);
    }

    override write(content: T, insertBreakAtEnd = true) {
        return super.write(content, insertBreakAtEnd);
    }

    protected treatContent2Record(content: T, insertBreakAtEnd = true): string {

        let _content: string = '';

        if (typeof content === 'object') {
            _content = JSON.stringify(content, undefined, 2);
        }

        if (insertBreakAtEnd) {
            _content = `${_content}\n`;
        }

        return _content;
    }

    protected treatObtainedContent(content: string): T | null {

        let resolvedContent: T | null = null;

        if (content) {
            resolvedContent = JSON.parse(content
                .replace(Helpers.EcmaScriptFilesType.resolveBlockCommentsPattern, '\n')
                .replace(Helpers.EcmaScriptFilesType.resolveLineCommentsPattern, '')
            );
        }

        return resolvedContent;
    }

}
