import { JsonFileHandler } from "../../../index.mjs";


const jsonHandler = new JsonFileHandler('./json.json');

const content = await jsonHandler.read();


if (content) {
    console.log('current content:', content);

    (content as any)['set2'] = 'val3';

    await jsonHandler.write(content);

    console.log('see the compiled json.json on the folder src/tests/json. The set2 key received a new value.')
}
