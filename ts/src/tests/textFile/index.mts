import { FileHandler } from "../../../index.mjs";

const fileHandler = new FileHandler('../textFile.txt');

const content = await fileHandler.read();

if (content) {
    console.log('current content:', content);

    // (content as any)['set2'] = 'val3';

    await fileHandler.write(`${content}`);

    // console.log('see the compiled testFile.json on the folder src/tests. The set2 key received a new value.')
}
