const {
  createCipheriv,
  createDecipheriv,
  getCiphers
  // , scryptSync
  // , createHash
} = await import('crypto');

/**
 *
 * @returns number[]
 */
String.prototype.toUtf8Bytes = function() {
  return Array.from((new TextEncoder()).encode(this));
}

String.prototype.toUtf8 = function() {
  return (new TextEncoder).encode(this);
}

class Base64 {

  static encode(text, encoding = 'utf-8') {
    return Buffer.from(text, encoding).toString('base64');
  }

  static decode(text, encoding = 'utf-8') {
    return Buffer.from(text, 'base64').toString(encoding);
  }

  static decodeAsBytes_likeDamnJava(text) {

    const buffer = Buffer.from(text, 'base64');

    let y;

    const bytes = [];

    for (let i=0; i < buffer.length; i++) {

      y = buffer[i];

      if (y > 127) {
        y = -(256 - y);
      }

      bytes.push(y);
    }

    return bytes;
  }

}

/*
// console.log(JSON.stringify(getCiphers()))

const encrypted = 'AIGBXYEWXz5w2Z3Fjqe5YiQbyR5eVbPW';
// console.log(Base64.decodeAsBytes_likeDamnJava(encrypted))

const encryptedAsBuffer = Buffer.from(encrypted, 'base64');

const credentials = 'abcdefghijklmnop';

// const credentialsSubstring = credentials.substring(0, credentials.length - (credentials.length - 16));
// console.log(credentialsSubstring)
// console.log(credentialsSubstring.length)

// const credentials = 'abcdefghijklmnop';

let credentialsAsBuffer  = Buffer.from(credentials, 'utf-8');
// console.log(credentialsAsBuffer)
// console.log(Buffer.concat([credentialsAsBuffer, credentialsAsBuffer]))
// let credentialsAsBuffer  = Buffer.from(credentialsSubstring, 'utf-8');
// console.log(credentialsAsBuffer.length) //=> 16

// credentialsAsBuffer = Buffer.concat([credentialsAsBuffer, credentialsAsBuffer]);

const strategy = 'des-ede-cbc';

// const iv = credentialsAsBuffer.subarray(credentialsAsBuffer.length - (credentialsAsBuffer.length - 8));
// const iv = Buffer.alloc(credentialsAsBuffer.length * 2);
const iv = Buffer.alloc(credentialsAsBuffer.length / 2);
// console.log(credentialsAsBuffer.length - (credentialsAsBuffer.length - 8))

const decipher = createDecipheriv(strategy, credentialsAsBuffer, iv);

// decipher.setAutoPadding(false);

// let decrypted = decipher.update(encryptedAsBuffer, 'base64', 'utf-8');
// decrypted += decipher.final('utf-8');
// console.log(decrypted);


function encrypt(text, credentials) {

  var key = Buffer.from(credentials, 'utf-8');
  var iv = key.subarray(key.length - (key.length - 8));
  // var iv = Buffer.alloc(8);

  var cipher = createCipheriv(strategy, key, iv);
  var encrypted = cipher.update(text, 'utf-8', 'base64');
  encrypted += cipher.final('base64');

  return encrypted;
}

function decrypt(encryptedText, credentials) {

  var encryptedAsBuffer = Buffer.from(encryptedText, 'base64');
  var key = Buffer.from(credentials, 'utf-8');
  var iv = key.subarray(key.length - (key.length - 8));
  // console.log(Buffer.alloc(8))

  var decipher = createDecipheriv(strategy, key, iv);
  var decrypted = decipher.update(encryptedAsBuffer, 'base64', 'utf-8');
  decrypted += decipher.final('utf-8');

  return decrypted;
}

var text = 'Carolina Freire dos Santos';
var securityKey = 'abcdefghijklmnop';
var encryptedText = encrypt(text, securityKey);
var decryptedText = decrypt(encryptedText, securityKey);
// var decryptedText = decrypt(encrypted, securityKey);

console.log('encrypted text:', encryptedText);
console.log('decrypted text:', decryptedText);
/**/

const encrypted = 'k04BWIELn56BZ+hwDeTL/YnH5d27I6+24G63yCc9aAE=';

const stringKey = 'Pr0v!d2R1t.BUsiNesS.S0LUt1oNs';
let keyAsBuffer = Buffer.from(stringKey, 'utf-8').subarray(0, 8); // cut the key
keyAsBuffer = Buffer.concat([keyAsBuffer, keyAsBuffer]); // DES key | DES key

const strategy = 'des-ede-ecb'; // Triple DES in 2TDEA variant, ECB mode
const decipher = createDecipheriv(strategy, keyAsBuffer, null);
let decrypted = decipher.update(encrypted, 'base64', 'utf-8');
decrypted += decipher.final('utf-8');

console.log(decrypted)
